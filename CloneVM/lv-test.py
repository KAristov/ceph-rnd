#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# (c) IBS Corp. 2019-2019
# created on platform: Linux karistov-mint-5400 5.0.0-32-generic #34~18.04.2-Ubuntu SMP Thu Oct 10 10:36:02 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux


import os
import sys
import libvirt

LVCS = "qemu:///system"

__author__='Konstantin "Kest" Aristov'
__date__='2019-12-10 11:54'

print("Do defined domain's list...")
conn_id=libvirt.open(LVCS)
names_ = conn_id.listDefinedDomains()
for row_ in names_:
    print("Domain: {0}".format(row_))
    dom_id=conn_id.lookupByName(row_)
    info_=dom_id.info()
    print("DomainID: {0}; DomainName: {1}; DomainState: {2}".format(dom_id, dom_id.name(), info_[0]))
    print("MaxMem: {0}; Number of virtCPU: {1}; CPU Time (ns): {2}".format(info_[1], info_[3], info_[2]))

print("Do detailed explain of running domain's...")
for id_ in conn_id.listDomainsID():
    dom_id = conn_id.lookupByID(id_)
    info_=dom_id.info()
    print("DomainID: {0}; DomainName: {1}; DomainState: {2}".format(id_, dom_id.name(), info_[0]))
    print("MaxMem: {0}; Number of virtCPU: {1}; CPU Time (ns): {2}".format(info_[1], info_[3], info_[2]))

sys.exit()


