#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# (c) IBS Corp. 2019-
# created on platform: Linux karistov-mint-5400 5.0.0-32-generic #34~18.04.2-Ubuntu SMP Thu Oct 10 10:36:02 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

import os
import sys
import libvirt
import guestfs
import subprocess

import common_func as c_f
import fileops as f_o
base_VMs=['altlinux8', 'centos7.0', 'ubuntu18.04']
LVCS = "qemu:///system"

__author__='Konstantin "Kest" Aristov'
__date__='2019-12-10 16:32'


if (len(sys.argv)!=2):
    print("Usage: {0} N".format(sys.argv[0]))
    print("where N:")
    count_=0
    for row_ in base_VMs:
        print("{0} for clone from {1}".format(count_, row_))
        count_+=1
    sys.exit()

items_=sys.argv[1]
try:
    eta_VM=base_VMs[int(items_)]
except TypeError as err_:
    print("Vrong argument type! TypeError raised: {0}".format(err_))
    sys.exit()
except IndexError as err_:
    print("Vrong VM item number! IndexError raised: {0}".format(err_))
    sys.exit()
except ValueError as err_:
    print("Vrong VM item value! ValueError raised: {0}".format(err_))
    sys.exit()

print("Ready to clone etalon VM: {0}".format(eta_VM))
print("Press ENTER to continue or Ctrl+C for abort clone.")
input()

print("Stage I. Check for condition.")
conn_id=libvirt.open(LVCS)
# Проверяем, что мы запущены от рута и исходная ВМ выключена
if (c_f.is_root()):
    print("Run as root - passed.")
else:
    print("We need root priveleged!")
    sys.exit()
if (c_f.check_domain_state(conn_id, eta_VM)):
    print("Domain {0} in true state for clone!".format(eta_VM))
else:
    print("Domain {0} in wrong state for clone!".format(eta_VM))
    sys.exit()
    
print("Stage II. Make clone ops.")
# Изготавливаем имена новых ВМ и в цикле (0..2) клонируем их
clone_name_list=list()
for id_ in range(3):
    new_domain_name="{0}-0{1}".format(eta_VM, id_+1)
    cmd_line="virt-clone --original {0} --name {1} --auto-clone".format(eta_VM, new_domain_name)
    clone_name_list.append(new_domain_name)
    print("Ready to execute: {0}".format(cmd_line))
    ret_code=subprocess.call(cmd_line, shell=True)
    print("Clone result: {0}".format(cmd_line))

print("Stage III. Upload file(s).")
count_=1
for row_ in clone_name_list:
    print("Ready to upload new settings to domain: {0}".format(row_))
    print("JobID: {0}".format(int(items_)))
    print("CloneVM: {0}".format(count_))
    local_files=c_f.make_upload_names(row_, count_)
    remote_files=c_f.make_remote_names(int(items_))
    print("Ready upload for domain: {0}".format(row_))
    print("IP-address setting from: {0} to {1}".format(local_files[0], remote_files[0]))
    print("Hostname setting from: {0} to {1}".format(local_files[1], remote_files[1]))
    res_upl=f_o.make_upload(row_, local_files, remote_files)
    print("Result upload is: {0}".format(res_upl))
        # Вызов функции аплоада
    #if ((f_o.check_localfile(local_files[0]!=None)) and (f_o.check_localfile(local_files[1]!=None))):

    count_+=1

    # Изготавливаем имена с шаблонами для настроек IP-адреса и hostname
# в цикле (0..2) аплоадим их на клонированные ВМ

sys.exit()
