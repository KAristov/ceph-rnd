#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# (c) IBS Corp. 2019-2019
# created on platform: Linux karistov-mint-5400 5.0.0-32-generic #34~18.04.2-Ubuntu SMP Thu Oct 10 10:36:02 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

import os
import sys
import guestfs
import fileops as f_o
import common_func as c_f

__author__='Konstantin "Kest" Aristov'
__date__='2019-12-10 15:18'



if (len(sys.argv)!=3):
    print("Usage: {0} DomainName File2Download".format(sys.argv[0]))
    sys.exit()

domain_name=sys.argv[1]
filename_2remote=sys.argv[2]
filename_2local=c_f.make_local_filename(domain_name, filename_2remote)

if (os.getuid()!=0):
    print("This operation try root privelegies!")
    sys.exit()
    
print("Ready to download file: {0} from remote: {1} for VM: {2}".format(filename_2local, filename_2remote, domain_name))
print("Press ENTER to continue or Ctrl+C to abort operations.")
input()

try:
    gfs_=guestfs.GuestFS(python_return_dict=True)
    print("LibugestFS handler created.")
    gfs_.add_domain(domain_name,readonly=1)
    print("Drive image(s) for domain {0} added.".format(domain_name))
    gfs_.launch()
    print("LibguestFS launched.")
    roots_=gfs_.inspect_os()
    res_root=f_o.inspect_os_from_image(gfs_,roots_)
    if (res_root!=None):
        gfs_.mount(res_root, '/') # Всегда используем фиксированную точку монтирования, ибо нехуй
        print("Filesystem from image mounted.")
        print("Ready to download file: {0} as {1}".format(filename_2remote, filename_2local))
        gfs_.download(filename_2remote,filename_2local)
        print("Download.")
        gfs_.umount_all()
        print("GuestFS unmounted.")
    else:
        print("Could not find usable root!")
except RuntimeError as err_str:
    print("Runtime error! Explain: {0}".format(err_str))
except IOError as e_:
    err_no, err_str = e_.args
    print("IOError, explain: ({1}) {2} ".format(FName, err_no, err_str))


sys.exit()
