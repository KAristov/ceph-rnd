#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# (c) IBS Corp. 2019-2019
# created on platform: Linux karistov-mint-5400 5.0.0-32-generic #34~18.04.2-Ubuntu SMP Thu Oct 10 10:36:02 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

import os
import sys
import guestfs
import fileops as f_o

__author__='Konstantin "Kest" Aristov'
__date__='2019-12-10 15:18'



if (len(sys.argv)!=4):
    print("Usage: {0} DomainName File2Upload FileOnVM".format(sys.argv[0]))
    sys.exit()

domain_name=sys.argv[1]
filename_2upload=sys.argv[2]
filename_2remote=sys.argv[3]

if (f_o.check_localfile(filename_2upload)==None):
    print("Could not work with {0}!".format(filename_2upload))
    sys.exit()

if (os.getuid()!=0):
    print("This operation try root privelegies!")
    sys.exit()
    
print("Ready to upload file: {0} as remote: {1} for VM: {2}".format(filename_2upload, filename_2remote, domain_name))
print("Press ENTER to continue or Ctrl+C to abort operations.")
input()

try:
    gfs_=guestfs.GuestFS(python_return_dict=True)
    print("LibugestFS handler created.")
    gfs_.add_domain(domain_name,readonly=0)
    print("Drive image(s) for domain {0} added.".format(domain_name))
    gfs_.launch()
    print("LibguestFS launched.")
    roots_=gfs_.inspect_os()
    res_root=f_o.inspect_os_from_image(gfs_,roots_)
    if (res_root!=None):
        gfs_.mount(res_root, '/') # Всегда используем фиксированную точку монтирования, ибо нехуй
        print("Filesystem from image mounted.")
        print("Ready to upload file: {0} as {1}".format(filename_2upload, filename_2remote))
        gfs_.upload(filename_2upload,filename_2remote)
        print("Uploaded.")
        gfs_.umount_all()
        print("GuestFS unmounted.")
    else:
        print("Could not find usable root!")
except RuntimeError as err_str:
    print("Runtime error! Explain: {0}".format(err_str))


sys.exit()
