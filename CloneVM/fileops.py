#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# (c) IBS Corp. 2019-2019
# created on platform: Linux karistov-mint-5400 5.0.0-32-generic #34~18.04.2-Ubuntu SMP Thu Oct 10 10:36:02 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

import sys,os,guestfs

__author__='Konstantin "Kest" Aristov'
__date__='2019-12-10 15:43'

def inspect_os_from_image(GFSHandler,GFSRoots):
    print("Inspect OS result:")
    count_=0
    try:
        for root in GFSRoots:
            print("Counter: {0}".format(count_))
            print("Root device: {0}".format(root))
            print("  Product name: {0}" .format(GFSHandler.inspect_get_product_name(root)))
            print("  Version:      {0}.{1}".format(GFSHandler.inspect_get_major_version(root),GFSHandler.inspect_get_minor_version(root)))
            print("  Type:         {0}".format(GFSHandler.inspect_get_type(root)))
            print("  Distro:       {0}".format(GFSHandler.inspect_get_distro(root)))
            count_+=1
    except RuntimeError as err_str:
        print("On step: {0} runtime error occured! Explain: {1}".format(count_,err_str))
        return None
    print("Total: {0} item(s) inspected.".format(count_))
    return root

def check_localfile(FName):
    try:
        f_d=open(FName, 'r')
        f_d.close()
    except OSError as e_:
        print("OSError on {0}: {1}".format(FName, e_))
        return None
    except IOError as e_:
        err_no, err_str = e_.args
        print("IOError on {0}: ({1}) {2} ".format(FName, err_no, err_str))
        return None
    return 1
    

def make_upload(DomainName, Files2Upload, Files2Remote):
    domain_name=DomainName
    filename_2upload=Files2Upload
    filename_2remote=Files2Remote
    count_=0
    try:
        gfs_=guestfs.GuestFS(python_return_dict=True)
        print("LibugestFS handler created.")
        gfs_.add_domain(domain_name,readonly=0)
        print("Drive image(s) for domain {0} added.".format(domain_name))
        gfs_.launch()
        print("LibguestFS launched.")
        roots_=gfs_.inspect_os()
        res_root=inspect_os_from_image(gfs_,roots_)
        if (res_root!=None):
            gfs_.mount(res_root, '/') # Всегда используем фиксированную точку монтирования, ибо нехуй
            print("Filesystem from image mounted.")
            print("Ready to upload file (IP-address settings): {0} as {1}".format(filename_2upload[0], filename_2remote[0]))
            gfs_.upload(filename_2upload[0],filename_2remote[0])
            print("Uploaded.")
            count_+=1
            print("Ready to upload file (hostname settings): {0} as {1}".format(filename_2upload[1], filename_2remote[1]))
            gfs_.upload(filename_2upload[1],filename_2remote[1])
            print("Uploaded.")            
            gfs_.umount_all()
            print("GuestFS unmounted.")
            count_+=1            
            return count_
        else:
            print("Could not find usable root!")
    except RuntimeError as err_str:
        print("Runtime error! Explain: {0}".format(err_str))
        return None
    return count_
