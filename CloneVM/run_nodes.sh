#!/bin/bash
# Run all nodes in cluster

if [ -n "$1" ]
then
  if [ $1 = 0 ]
  then
    virsh start altlinux8-01
    virsh start altlinux8-01
    virsh start altlinux8-01
  else
    if [ $1 == 1 ]
    then
      virsh start centos7.0-01
      virsh start centos7.0-02
      virsh start centos7.0-03
    else
      if [ $1 == 2 ]
      then
        virsh start ubuntu18.04-01
        virsh start ubuntu18.04-02
        virsh start ubuntu18.04-03
      else
        echo "Wrong param value $1!"
      fi
    fi
  fi
else
  echo "Usage $0 N, where N:"
  echo "0 for AltLinux run"
  echo "1 for CentOS run"
  echo "2 for UbuntuServer run"
fi
