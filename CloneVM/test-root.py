#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# (c) IBS Corp. 2019-2019
# created on platform: Linux karistov-mint-5400 5.0.0-32-generic #34~18.04.2-Ubuntu SMP Thu Oct 10 10:36:02 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

import os
import sys

__author__='Konstantin "Kest" Aristov'
__date__='2019-12-10 12:42'

curr_uid=os.getuid()
print("CurrentID: {0}".format(curr_uid))
sys.exit()
