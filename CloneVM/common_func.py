#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# (c) IBS Corp. 2019-2019
# created on platform: Linux karistov-mint-5400 5.0.0-32-generic #34~18.04.2-Ubuntu SMP Thu Oct 10 10:36:02 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux

import os
import sys
import libvirt

__author__='Konstantin "Kest" Aristov'
__date__='2019-12-10 16:07'

def make_local_filename(DomainName, RemoteFileName):
    remote_arr = RemoteFileName.split('/')
    ret_str=DomainName
    for row_ in remote_arr:
        ret_str+='-{0}'.format(row_)

    return ret_str

def is_root():
    if (os.getuid()==0):
        return True
    else:
        return False

def check_domain_state(ConnID, DomainName):
    try:
        dom_id=ConnID.lookupByName(DomainName)
    except libvirt.libvirtError as err_:
        print("Could not find domain: {0}".format(DomainName))
        print("libvirtError raised: {0}".format(err_))
        return False
    info_=dom_id.info()
    domain_status=info_[0]
    if (domain_status!=5):
        print("Domain: {0}; status: {1}".format(DomainName, domain_status))
        return False
    else:        
        return True
    
def make_upload_names(EtaVMName, EtaVMID):
    path_ip="CloneData/{0}-ip".format(EtaVMName)
    path_hostname="CloneData/{0}-hostname".format(EtaVMName)
    return (path_ip, path_hostname)

def make_remote_names(ID):
    if (ID==0):
        ip_addr="/etc/net/ifaces/ens3/ipv4address"
    if (ID==1):
        ip_addr="/etc/sysconfig/network-scripts/ifcfg-eth0"
    if (ID==2):
        ip_addr="/etc/netplan/50-cloud-init.yaml"
    host_name="/etc/hostname"
    return (ip_addr, host_name)
