01-preflight.yml - предварительная подстройка ОС перед установкой Ceph (подключение репозиториев etc)
02-install-ceph.yml - установка дополнительных пакетов и самого ceph
03-create-ceph-conf.yml - создание файла конфигурации кластера /etc/ceph/ceph.conf
    ВНИМАНИЕ!!! Запуск данного плейбука:
ansible-playbook -v --extra-vars="target=ceph-cluster-centos cluster_id="`uuidgen` 03-create-ceph-conf.yml
    то есть cluster_id мы получаем из внешней команды uuidgen
04-create-keyrings.yml - создание ключей авторизации и пользователей внтури кластера
Стоп, нефига, поскольку в кейрингах фигурирует ClusterID - его надо либо брать из файла конфигурации, либо делать одним плейбуком с файлом конфигурации /etc/ceph/ceph.conf
    Пойдем по второму пути
03-create-keyrings-n-conf.yml - выполняем этот плейбук для одной ноды!!!
    
    