Команда:
sudo fio -ioengine=libaio -direct=1 -invalidate=1 -name=test -bs=4k -iodepth=128 -rw=randread -runtime=60 -filename=/dev/nvme0n1p5
fio-3.1
Starting 1 process
Jobs: 1 (f=1): [r(1)][100.0%][r=802MiB/s,w=0KiB/s][r=205k,w=0 IOPS][eta 00m:00s]
test: (groupid=0, jobs=1): err= 0: pid=17062: Thu Dec 12 14:20:13 2019
   read: IOPS=177k, BW=693MiB/s (726MB/s)(5000MiB/7217msec)
    slat (nsec): min=803, max=2980.1k, avg=4133.90, stdev=11205.82
    clat (usec): min=113, max=15190, avg=716.52, stdev=383.47
     lat (usec): min=128, max=15195, avg=720.79, stdev=384.87
    clat percentiles (usec):
     |  1.00th=[  338],  5.00th=[  392], 10.00th=[  420], 20.00th=[  469],
     | 30.00th=[  515], 40.00th=[  562], 50.00th=[  611], 60.00th=[  668],
     | 70.00th=[  742], 80.00th=[  865], 90.00th=[ 1172], 95.00th=[ 1467],
     | 99.00th=[ 2114], 99.50th=[ 2343], 99.90th=[ 2933], 99.95th=[ 3294],
     | 99.99th=[ 7111]
   bw (  KiB/s): min=563512, max=853768, per=99.67%, avg=707069.71, stdev=114285.69, samples=14
   iops        : min=140878, max=213442, avg=176767.43, stdev=28571.42, samples=14
  lat (usec)   : 250=0.07%, 500=26.84%, 750=43.88%, 1000=14.87%
  lat (msec)   : 2=12.92%, 4=1.39%, 10=0.01%, 20=0.01%
  cpu          : usr=26.18%, sys=49.09%, ctx=48484, majf=0, minf=136
  IO depths    : 1=0.1%, 2=0.1%, 4=0.1%, 8=0.1%, 16=0.1%, 32=0.1%, >=64=100.0%
     submit    : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.0%
     complete  : 0=0.0%, 4=100.0%, 8=0.0%, 16=0.0%, 32=0.0%, 64=0.0%, >=64=0.1%
     issued rwt: total=1280000,0,0, short=0,0,0, dropped=0,0,0
     latency   : target=0, window=0, percentile=100.00%, depth=128

Run status group 0 (all jobs):
   READ: bw=693MiB/s (726MB/s), 693MiB/s-693MiB/s (726MB/s-726MB/s), io=5000MiB (5243MB), run=7217-7217msec

Disk stats (read/write):
  nvme0n1: ios=1267061/64, merge=0/7, ticks=598070/18, in_queue=1808, util=98.77%
